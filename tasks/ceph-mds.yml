- name: create mds data dir
  file:
    path: /var/lib/ceph/mds/ceph-{{ inventory_hostname }}
    state: directory

- name: create mds keyring
  shell: >
    ceph-authtool --create-keyring 
    /var/lib/ceph/mds/ceph-{{ inventory_hostname }}/keyring 
    --gen-key -n mds.{{ inventory_hostname }}
  args:
    creates: /var/lib/ceph/mds/ceph-{{ inventory_hostname }}/keyring 

- name: setup mds keyring permissions
  shell: >
    ceph auth add mds.{{ inventory_hostname }} 
    osd "allow rwx" 
    mds "allow" 
    mon "allow profile mds" 
    -i /var/lib/ceph/mds/ceph-{{ inventory_hostname }}/keyring

- name: add mds to ceph config
  blockinfile:
    marker: "# ANSIBLE MDS CONFIG {mark}"
    path: /etc/ceph/ceph.conf
    block: |
      [mds.{{ inventory_hostname }}]
      host = {{ inventory_hostname }}

- name: ensure correct permissions on ceph runtime dir
  file:
    path: /var/lib/ceph
    owner: ceph
    group: ceph
    recurse: yes

- name: start ceph mds
  systemd:
    name: ceph-mds@{{ inventory_hostname }}
    state: started
    enabled: true

- name: create the mergefs data pool
  shell: ceph osd pool create {{ ceph.cephfs.data_pool }} 64

- name: create the mergefs metadata pool
  shell: ceph osd pool create {{ ceph.cephfs.meta_pool }} 64

- name: create mergefs
  shell: ceph fs new {{ ceph.cephfs.name }} {{ ceph.cephfs.meta_pool }} {{ ceph.cephfs.data_pool }}

# Crush rules determine how the CRUSH algorithm will move data within
# the cluster to prevent erasure failures due to domain failures. For example
# is the domain of failure is expected to be: disk (osd) level, host level, rack level
# or region level.  By default crush will do host level domain failures.
# Docs: docs.ceph.com/docs/master/rados/operations/crush-map
- name: create appropriate crush map
  shell: ceph osd crush rule create-replicated osd-replicated default osd
  when: ceph.crush is defined and ceph.crush.osd is defined and ceph.crush.osd

- name: set data pool crush rule
  shell: ceph osd pool set {{ ceph.cephfs.data_pool }} crush_rule osd-replicated
  when: ceph.crush is defined and ceph.crush.osd is defined and ceph.crush.osd

- name: set metadata pool crush rule
  shell: ceph osd pool set {{ ceph.cephfs.meta_pool }} crush_rule osd-replicated
  when: ceph.crush is defined and ceph.crush.osd is defined and ceph.crush.osd
