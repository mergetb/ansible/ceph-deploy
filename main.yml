- hosts: "{{ services.servers + services.clients }}"
  become: true
  tasks:
    - name: update all servers
      apt:
        update_cache: yes
        upgrade: dist
    - name: install ceph packages
      apt:
        name:
          - ceph
          - ceph-base
          - ceph-mds
          - ceph-mgr
          - ceph-mgr-dashboard

# configure the first initial node, responsible for setting up
# all the ceph settings (getting monitor online)
# NOTE: this configuration requires that atleast one node is
# responsible for running all services. it will be chosen
# based on the first entry in ceph.mons.
- hosts: "{{ ceph.mons | sort | first }}"
  become: true
  tasks:
    - include_tasks: tasks/ceph-mon.yml

- hosts: "{{ ceph.mons | sort | first }}"
  become: true
  tasks:
    - include_tasks: tasks/ceph-mds.yml

- hosts: "{{ ceph.mons | sort | first }}"
  become: true
  tasks:
    - include_tasks: tasks/ceph-osd.yml

- hosts: "{{ ceph.mons | sort | first }}"
  become: true
  tasks:
    - include_tasks: tasks/ceph-mgr.yml

# get the ceph key material
- hosts: localhost
  become: true
  tasks:
    - name: remove local admin keyring
      file:
        path: ./ceph.client.admin.keyring
        state: absent

# retrieve from the initial monitor the admin key and ceph
# configuration file.
- hosts: "{{ ceph.mons | sort | first }}"
  become: true
  tasks:
    - name: get ceph key for non-mons
      fetch:
        src: /etc/ceph/ceph.client.admin.keyring
        dest: ./ceph.client.admin.keyring
        flat: yes
    - name: get ceph conf
      fetch:
        src: /etc/ceph/ceph.conf
        dest: ./ceph.conf
        flat: yes

# put the key and configuration file on the clients accessing ceph
# as well as the servers which are not running monitors
- hosts: "{{ services.clients | union(services.servers | difference(ceph.mons | list)) }}"
  become: true
  tasks:
    - name: put ceph key on non-mons
      copy:
        src: ./ceph.client.admin.keyring
        dest: /etc/ceph/ceph.client.admin.keyring
        owner: root
        group: ceph
        mode: "0640"
    - name: put ceph conf on non-mons
      copy:
        src: ./ceph.conf
        dest: /etc/ceph/ceph.conf
        owner: root
        group: ceph
        mode: "0640"

# on non-monitor nodes we will then need to generate a boostrap osd key
- hosts: "{{ services.servers | difference(ceph.mons | sort | first | list) }}"
  become: true
  tasks:
    - name: get osd bootstrap key
      shell: ceph auth get client.bootstrap-osd > /var/lib/ceph/bootstrap-osd/ceph.keyring
      become: yes

# for every other node we add to the monitors using the already
# generated key material
- hosts: "{{ ceph.mons | difference(ceph.mons | sort | first | list) }}"
  become: true
  tasks:
    - include_tasks: tasks/ceph-mon-add.yml

- hosts: "{{ ceph.mds | difference(ceph.mons | sort | first | list) }}"
  become: true
  tasks:
    - include_tasks: tasks/ceph-mds.yml

- hosts: "{{ services.servers | difference(ceph.mons | sort | first | list) }}"
  become: true
  tasks:
    - include_tasks: tasks/ceph-osd.yml

- hosts: "{{ ceph.mgrs | difference(ceph.mons | sort | first | list) }}"
  become: true
  tasks:
    - include_tasks: tasks/ceph-mgr.yml

# only need to do this once, which is to reset the pg_num in the pools
- hosts: "{{ services.servers | sort | first }}"
  become: true
  tasks:
    - include_tasks: tasks/ceph-mds-set.yml
    - include_tasks: tasks/ceph-mgr-dashboard.yml
